
var listStaff = [];

var staffFromLocalStorage = localStorage.getItem('LIST_STAFF');

if (staffFromLocalStorage != null) {
    var staffToConvert = JSON.parse(staffFromLocalStorage);
    var listStaff = staffToConvert.map(function (item) {
        var staff = new Staff(item.account, item.name, item.email, item.password, item.datePicker, item.salary, item.position, item.workTime)
        return staff;
    })
    renderStaff(listStaff);
}

function addStaff() {
    var infomationStaff = layThongTinTuForm()
    listStaff.push(infomationStaff);

    var stringifyStaff = JSON.stringify(listStaff)

    renderStaff(listStaff);

    localStorage.setItem('LIST_STAFF', stringifyStaff)
    $('#myModal').modal('hide');
}

function removeStaff(_index) {
    var indexStaffToRemove = -1
    indexStaffToRemove = findIndex(_index, listStaff)

    if (indexStaffToRemove != -1) {
        listStaff.splice(indexStaffToRemove, 1)
    }
    renderStaff(listStaff);
}

function updateStaff(_index) {
    var indexStaffToUpDate = -1
    indexStaffToUpDate = findIndex(_index, listStaff)
    var eleStaff = listStaff[indexStaffToUpDate]
    if (indexStaffToUpDate != -1) {
        $('#myModal').modal('show');
        document.getElementById('tknv').value = eleStaff.account;
        document.getElementById('name').value = eleStaff.name;
        document.getElementById('email').value = eleStaff.email;
        document.getElementById('password').value = eleStaff.password;
        document.getElementById('datepicker').value = eleStaff.datePicker;
        document.getElementById('luongCB').value = eleStaff.salary;
        document.getElementById('chucvu').value = eleStaff.position;
        document.getElementById('gioLam').value = eleStaff.workTime;
    }

}

function updateSavingStaff() {
    var infomationStaff = layThongTinTuForm()

    var indexStaffToUpDate = -1
    indexStaffToUpDate = findIndex(infomationStaff.account, listStaff)
    var eleStaff = listStaff[indexStaffToUpDate]
    if (indexStaffToUpDate != -1) {
        listStaff[indexStaffToUpDate] = infomationStaff
        renderStaff(listStaff)
        $('#myModal').modal('hide');
    }
}