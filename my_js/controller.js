function Staff(_account, _name, _email, _password, _datePicker, _salary, _position, _workTime) {
    this.account = _account;
    this.name = _name;
    this.email = _email;
    this.password = _password;
    this.datePicker = _datePicker;
    this.salary = _salary;
    this.position = _position;
    this.workTime = _workTime;
    this.countTotalSalary = function () {
        if (this.position == 'Sếp') {
            return this.salary * 3
        }
        else if (this.position == 'Trưởng phòng') {
            return this.salary * 2
        }
        else {
            return this.salary * 1
        }
    };
    this.classifyStaff = function () {
        if (this.workTime >= 196) {
            return 'Nhân viên suất xắc'
        }
        else if (this.workTime >= 176) {
            return 'Nhân viên giỏi'
        }
        else if (this.workTime >= 160) {
            return 'Nhân viên khá'
        }
        else {
            return 'Nhân viên trung bình'
        }
    }
}

function layThongTinTuForm() {
    var _account = document.getElementById('tknv').value;
    var _name = document.getElementById('name').value;
    var _email = document.getElementById('email').value;
    var _password = document.getElementById('password').value;
    var _datePicker = document.getElementById('datepicker').value;
    var _salary = document.getElementById('luongCB').value * 1;
    var _position = document.getElementById('chucvu').value;
    var _workTime = document.getElementById('gioLam').value * 1;

    var staff = new Staff(_account, _name, _email, _password, _datePicker, _salary, _position, _workTime)

    return staff;
}

function renderStaff(_listStaff) {
    var contentStaff = ''
    _listStaff.forEach(ele => {
        contentStaff += `
            <tr>
                <th>${ele.account}</th>
                <th>${ele.name}</th>
                <th>${ele.email}</th>
                <th>${ele.datePicker}</th>
                <th>${ele.position}</th>
                <th>${ele.countTotalSalary()}</th>
                <th>${ele.classifyStaff()}</th>
                <th>
                    <button class="btn btn-success" onclick="updateStaff('${ele.account}')">Sửa</button>
                    <button class="btn btn-danger mt-2" onclick="removeStaff('${ele.account}')">Xóa</button>
                </th>
            </tr>
        `
    })
    document.getElementById('tableDanhSach').innerHTML = contentStaff
}

function findIndex(_index, _listStaff) {
    var index
    _listStaff.forEach(ele => {
        if (ele.account === _index) {
            index = _listStaff.indexOf(ele)

        }
    })
    return index
    // console.log(ele.findIndex(_index))
}

function layThongTinTuForm() {
    var _account = document.getElementById('tknv').value;
    var _name = document.getElementById('name').value;
    var _email = document.getElementById('email').value;
    var _password = document.getElementById('password').value;
    var _datePicker = document.getElementById('datepicker').value;
    var _salary = document.getElementById('luongCB').value * 1;
    var _position = document.getElementById('chucvu').value;
    var _workTime = document.getElementById('gioLam').value * 1;

    var staff = new Staff(_account, _name, _email, _password, _datePicker, _salary, _position, _workTime)

    return staff;
}

function renderStaff(_listStaff) {
    var contentStaff = ''
    _listStaff.forEach(ele => {
        contentStaff += `
            <tr>
                <th>${ele.account}</th>
                <th>${ele.name}</th>
                <th>${ele.email}</th>
                <th>${ele.datePicker}</th>
                <th>${ele.position}</th>
                <th>${ele.countTotalSalary()}</th>
                <th>${ele.classifyStaff()}</th>
                <th>
                    <button class="btn btn-success" onclick="updateStaff('${ele.account}')">Sửa</button>
                    <button class="btn btn-danger mt-2" onclick="removeStaff('${ele.account}')">Xóa</button>
                </th>
            </tr>
        `
    })
    document.getElementById('tableDanhSach').innerHTML = contentStaff
}

function findIndex(_index, _listStaff) {
    var index
    _listStaff.forEach(ele => {
        if (ele.account === _index) {
            index = _listStaff.indexOf(ele)

        }
    })
    return index
    // console.log(ele.findIndex(_index))
}
